var timer = require("./index").timer;


class myclass{

    constructor(){
        this.a = 10;
        this.b = 2;
    }

    incA(){
        this.a++;
        console.log(this.a);
    }

    incB(){
        this.b++;
        console.log(this.b);
    }

}

var myobj = new myclass();



var tim1 = new timer(() =>{
    myobj.incA();
},1000);

tim1.start();

tim1.onInit(function(){
    console.log("onInit");
});
tim1.onBegin(function(){
    console.log("onBegin");
});
tim1.onFinish(function(){
    console.log("onFinish");
});
tim1.onClose(function(){
    console.log("closed");
});


setTimeout(() => {
    console.log("STOP TRIGGER");
    tim1.stop();
},5000)



