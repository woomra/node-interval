var _ = require('lodash');
var timer_ = require('./timer');

class interval{

    constructor () {
        this.timers = [];
    }

    addTimer(fnc,ms){
        let timer = new timer_(fnc,ms);
        this.timers.push(timer);
        return this.timers[(this.timers.length -1)];
    }

    startTimer(timer){
        timer.start();
    }

    stopTimer(timer){
        timer.stop();
    }

    startTimers(){
        _.forEach(this.timers, (timer) =>{
            timer.start();
        });
    }

    stopTimers(){
        _.forEach(this.timers, (timer) =>{
            timer.stop();
        });
    }

};


module.exports = new interval();
