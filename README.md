# boss-intervals

If you need to manage intervals/timers/loops ( expecially on raspberry side, 
it's specially useful in use in graceful shutdown of the process ) this module is for you . 
with this simple module you can create intervals and timers and have a common manager for everythings.
.

An example to use in graceful shutdown

```js
var intervals = require("boss-intervals");

intervals.addTimer(() => {
    console.log("timer 1");
},1000);

intervals.addTimer(() => {
    console.log("timer 2");
},200);

intervals.startTimers();

process.on('SIGINT',() => {
	intervals.stopTimers();
	process.exitCode = 0;
});
```

example with timeout

```js
var intervals = require("boss-intervals");

intervals.addTimer(() => {
    console.log("timer 1");
},1000);

intervals.addTimer(() => {
    console.log("timer 2");
},200);

intervals.startTimers();

setTimeout(() => {
	intervals.stopTimers();
},5000);
```

You can also use a special events in every intervals/timers

```js
var intervals = require("boss-intervals");

var timer = intervals.addTimer(() => {
    console.log("timer 1");
},1000);

// Before the timer is initializated and ready to start
timer.onInit(function(){
    console.log("onInit");
});

// before the first line of code of your function (for every interaction)
timer.onBegin(function(){
    console.log("onBegin"); 
});

// after the last line of code of your function (for every interaction)
timer.onFinish(function(){ 
    console.log("onFinish");
});

// when the timer is terminated ( if you shutdown the timer 
// when it is ongoing the timer will be terminated and than 
// will be catched this event )
timer.onClose(function(){ 
    console.log("closed");
});


setTimeout(() => {
	intervals.stopTimers();
},10000);
```

The example below show how you can manage if a variable is free to use 

```js
var intervals = require("boss-intervals");
var a = 1;

var timer_1 = intervals.addTimer(() => {
    a++;
    console.log(a);
},1);

var timer_2 = intervals.addTimer(() => {
    if(timer_1.isRunning()) //Check if a timer is running
        console.log("var a it's busy from timer_1");
    else
        a = 1;
},1);
```

BYE BYE GUYS

