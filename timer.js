class timer_{

    constructor (fnc,ms) {

        this.fnc = fnc;
        this.ms = ms;
        this.interval = null;
        this.stopping = false;
        this.running = false;

        this.clk_init = () => {};
        this.clk_begin = () => {};
        this.clk_finish = () => {};
        this.clk_close = () => {};

    }

    isRunning(){
        if(this.running) return true;
        else return false;
    }

    start(){
        this.clk_init();

        let func_operation = () => {
            this.running = true;
            this.clk_begin();
            this.fnc();
            this.clk_finish();
            this.running = false;
            if(this.stopping){
                this.stop();
            }
        }

        func_operation();
        this.interval = setInterval(func_operation,this.ms);
    }

    stop(){
        if(!this.running){
            clearInterval(this.interval);
            this.clk_close();
            this.stopping = false;
        }
        else
            this.stopping = true;
    }

    onInit(fnc){
        this.clk_init = fnc;
    };
    onBegin(fnc){
        this.clk_begin = fnc;
    };
    onFinish(fnc){
        this.clk_finish = fnc;
    };
    onClose(fnc){
        this.clk_close = fnc;
    };


}

module.exports = timer_;
